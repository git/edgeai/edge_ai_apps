.. _pub_edgeai_running_advance_demos:

=====================
Running Advance demos
=====================

The same Python and C++ demo applications can be used to run multiple inference
models and also work with multiple inputs with just simple changes in the config
file.

From a repo of input sources, output sources and models one can define advance
dataflows which connect them in various configurations. Details on configuration
file parameters can be found in :ref:`pub_edgeai_configuration`

Single input multi inference demo
---------------------------------

Here is an example of a single-input, multi-inference demo which takes a camera
input and run multiple networks on each of them.

.. code-block:: bash

    root@tda4vm-sk:/opt/edge_ai_apps/apps_python# ./app_edgeai.py ../configs/single_input_multi_infer.yaml

Sample output for single input, multi inference demo is as shown below,

.. figure:: ./images/edgeai-single-input-multi-infer.jpg
   :align: center

   Sample output showing single input, mutli-inference output

We can specify the output window location and sizes as shown in the
configuration file,

.. code-block:: yaml

    flows:
        flow0: [input0,model0,output0,[160,90,800,450]]
        flow1: [input0,model1,output0,[960,90,800,450]]
        flow2: [input0,model2,output0,[160,540,800,450]]
        flow3: [input0,model3,output0,[960,540,800,450]]

Multi input multi inference demo
--------------------------------

Here is an example of a multi-input, multi-inference demo which takes a camera
input and video input and runs multiple networks on each of them.

.. code-block:: bash

    root@tda4vm-sk:/opt/edge_ai_apps/apps_python# ./app_edgeai.py ../configs/multi_input_multi_infer.yaml

Sample output for multi input, multi inference demo is as shown below,

.. figure:: ./images/edgeai-multi-input-multi-infer.jpg
   :align: center

   Sample output showing multi-input, mutli-inference output

We can specify the output window location and sizes as shown in the
configuration file,

.. code-block:: yaml

    flows:
        flow0: [input0,model1,output0,[160,90,800,450]]
        flow1: [input0,model2,output0,[960,90,800,450]]
        flow2: [input1,model0,output0,[160,540,800,450]]
        flow3: [input1,model3,output0,[960,540,800,450]]

We can also share the same model instance to do same inference on differnt
inputs. This will reduce the memory foot prints when compred to creating
multiple instace of same model. Below is an example for the same,

.. code-block:: yaml

    flows:
        flow0: [input0,model1,output0,[160,90,800,450]]
        flow1: [input0,model2,output0,[960,90,800,450]]
        flow2: [input1,model1,output0,[160,540,800,450]]
        flow3: [input1,model2,output0,[960,540,800,450]]

Here model1 and model2 is shared across.
