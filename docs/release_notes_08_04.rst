==============================
Release notes version 08.04.00
==============================

.. _pub_edgeai_new_in_this_release_08_04:

New in this Release
===================
 - SDK comes pre-installed with required dependencies and demo applications
 - Enable zero-copy in between GStreamer and runtimes
 - Support DMABUF import for v4l2 raw camera sources using tiovxisp element
 - Cross compile support for all EdgeAI repos
 - Support for separately enabling or disabling DL runtime
 - Support for streaming the demo output over the network
 - Added scripts to report stats on core loading and GStreamer pipeline
 - Support for SDE and DOF GStreamer plugins

.. _pub_fixed_in_this_release_08_04:

Fixed in this Release
=====================
 - ADASVISION-5635 - tiovxisp output is wrong with dmabuf-import when sensor resolution is not multiple of 64
 - ADASVISION-5584 - Edge AI demo: Image classification demo freezes around 98~99 frames
 - ADASVISION-5583 - Edge AI demo: file input in multi-output demo causes demo to freeze
 - ADASVISION-5494 - Add support for simultaneous use of RPi camera and Rpi extension header
 - ADASVISION-5475 - J7 SK: The setup_script.sh fails when the board is configured in AP mode, which is the default with WiFi only case
 - ADASVISION-5463 - ONR-OD-8000 model fails with IMX390 in CPP app due to 1200x1200 resolution
 - ADASVISION-5349 - Setup script fails building without internet connection
 - ADASVISION-4997 - Multi-input multi-inference demo freezes after long duration run

  .. _pub_edgeai_known_issues_08_04:

Known Issues
============
 - ADASVISION-5643 - Mosaic hangs in edge ai apps multi-input multi-inference when run with both instances of MSC
 - ADASVISION-5631 - tiovx mosaic doesn't work if the pixel ratio of input videos is different
 - ADASVISION-5447 - Models failing due to tiovxdlcolorconvert limitation
 - ADASVISION-5420 - No valid frames found before end of stream Image Input -> Video File Write

.. _pub_edgeai_model_zoo_status_08_04:

Model Zoo
=========
About 83 models from 8.4 model zoo are regressed with multiple input sources such as
camera and video files. A list of unsupported models are as below,

 - Height/2 is odd, tiovxdlpreproc block fetch logic fails
    - TFL-CL-0100-efficientNet-edgeTPU-m
    - TFL-CL-0140-efficientNet-lite4
    - TFL-CL-0170-efficientNet-lite1
    - TFL-CL-0190-efficientNet-edgeTPU-l

 - High latency causes DSS underflow
    - ONR-SS-8730-deeplabv3-mobv3-lite-large-cocoseg21-512x512

 - Odd resolution, tiovxdlcolorconvert fails
    - TVM-CL-3430-gluoncv-mxnet-xception
    - TFL-CL-0040-InceptionNetV3

 - Human pose model with unsupported post processing
    - ONR-KD-7000-human-pose-ae-mobv2-fpn-spp-udp-512x512
    - ONR-KD-7010-human-pose-ae-res50v2-fpn-spp-udp-512x512
    - ONR-KD-7020-human-pose-ae-mobv2-pan-spp-udp-512x512
    - ONR-KD-7030-human-pose-ae-res50v2-pan-spp-udp-512x512

.. note::
    Please also refer to `edgeai-gst-plugins issues <https://github.com/TexasInstruments/edgeai-gst-plugins/issues>`_
    for more details on bugs filed on custom GStreamer plugins

.. _pub_edgeai_software_components_08_04:

Software components
===================

List of software components used in this version

+------------------------------+---------------------+
| Componenet                   | Version             |
+==============================+=====================+
| PSDKLA (filesystem, uboot)   | 08.04.00.11         |
+------------------------------+---------------------+
| PSDKRA (firmware)            | 08.04.00.06         |
+------------------------------+---------------------+
| Docker                       | 19.03.8-ce          |
+------------------------------+---------------------+
| Ubuntu (Docker)              | 20.04.LTS           |
+------------------------------+---------------------+
| Python                       | 3.8                 |
+------------------------------+---------------------+
| OpenCV                       | 4.5.1               |
+------------------------------+---------------------+
| GStreamer                    | 1.16.2              |
+------------------------------+---------------------+
| Cmake                        | 3.16.3              |
+------------------------------+---------------------+
| Ninja                        | 1.10.0              |
+------------------------------+---------------------+
| Meson                        | 0.57.2              |
+------------------------------+---------------------+
| Jupyterlab                   | 3.0.14              |
+------------------------------+---------------------+
| NeoAI - DLR                  | 1.8.0               |
+------------------------------+---------------------+
| Tensorflow                   | TIDL_PSDK_8.4       |
+------------------------------+---------------------+
| TFLite-runtime               | TIDL_PSDK_8.4       |
+------------------------------+---------------------+
| ONNX-runtime                 | TIDL_PSDK_8.4       |
+------------------------------+---------------------+
| PyYAML                       | 5.4.1               |
+------------------------------+---------------------+
| TI Model Zoo                 | 8.4.0               |
+------------------------------+---------------------+
| edgeai-gst-plugins           | 0.7.1               |
+------------------------------+---------------------+
| edgeai-tiovx-modules         | 8.4.1               |
+------------------------------+---------------------+
| edgeai-tidl-tools            | 8.4.0.2             |
+------------------------------+---------------------+
