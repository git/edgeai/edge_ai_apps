==============================
Release notes version 08.05.00
==============================

.. _pub_edgeai_new_in_this_release:

New in this Release
===================
 - Added DL-Inferer GStreamer plugin which can be called from gst-launch command
 - Added DL-postproc GStreamer plugin which handles text overlay, drawing boxes, lines etc. natively in NV12 format
 - Added perfOverlay GStreamer plugin which overlays CPU load, HWA load, DDR bandwidth on the post-processed image
 - Added multi platform support which detects underlying platform at runtime
 - Added split-output support in edge_ai_apps to channel post-processed output to both encode and display
 - DL-Inferer component in edge_ai_apps is migrated to separate repo supporting TFLite RT, ONNX-RT and NeoAI-DLR

Fixed in this Release
=====================
 - EDGEAI_ROBOTICS-818 - TVM Models when used with file input does not exit cleanly
 - EDGEAI_ROBOTICS-814 - addr[0] is INVALID error reported by edge_ai_apps infrequently
 - EDGEAI_ROBOTICS-730 - Performance Visualization Tool command typo/error
 - EDGEAI_ROBOTICS-666 - No valid frames found before end of stream for image file input and video file output
 - EDGEAI_ROBOTICS-639 - tiovx mosaic plugin throws error if the pixel ratio of input videos is different
 - EDGEAI_ROBOTICS-335 - Issue with running setup script on HOST due to ldconfig

.. _pub_edgeai_known_issues:

Known Issues
============
 - EDGEAI_ROBOTICS-820 - Video container formats of .mp4 and .mov fails while writing in Docker
 - EDGEAI_ROBOTICS-819 - tiovxisp element infrequently throws error while exiting edge_ai_apps
 - EDGEAI_ROBOTICS-648 - tiovxdlcolorconvert fails when the image height/2 is not an even number
 - EDGEAI_ROBOTICS-642 - Mosaic element hangs when both instances of MSC are used in multi-input multi-inference usecase
 - EDGEAI_ROBOTICS-640 - LDC introduces image format while working with 1936 x 1096 resolution

.. _pub_edgeai_model_zoo_status:

Model Zoo
=========
Models from edgeai-modelzoo are regressed with multiple input sources such as
camera and video files. A list of unsupported models are as below,

 - Height/2 is odd, tiovxdlpreproc block fetch logic fails
    - TFL-CL-0100-efficientNet-edgeTPU-m
    - TFL-CL-0140-efficientNet-lite4
    - TFL-CL-0170-efficientNet-lite1
    - TFL-CL-0190-efficientNet-edgeTPU-l

 - High latency causes DSS underflow
    - ONR-SS-8730-deeplabv3-mobv3-lite-large-cocoseg21-512x512

 - Odd resolution, tiovxdlcolorconvert fails
    - TVM-CL-3430-gluoncv-mxnet-xception
    - TFL-CL-0040-InceptionNetV3

 - Human pose model with unsupported post processing
    - ONR-KD-7000-human-pose-ae-mobv2-fpn-spp-udp-512x512
    - ONR-KD-7010-human-pose-ae-res50v2-fpn-spp-udp-512x512
    - ONR-KD-7020-human-pose-ae-mobv2-pan-spp-udp-512x512
    - ONR-KD-7030-human-pose-ae-res50v2-pan-spp-udp-512x512

.. note::
    Please also refer to `edgeai-gst-plugins issues <https://github.com/TexasInstruments/edgeai-gst-plugins/issues>`_
    for more details on bugs filed on custom GStreamer plugins

.. _pub_edgeai_software_components:

Software components
===================

List of software components used in this version

+------------------------------+---------------------+
| Componenet                   | Version             |
+==============================+=====================+
| PSDKLA (filesystem, uboot)   | 08.05.00.08         |
+------------------------------+---------------------+
| PSDKRA (firmware)            | 08.05.00.11         |
+------------------------------+---------------------+
| Docker                       | 19.03.8-ce          |
+------------------------------+---------------------+
| Ubuntu (Docker)              | 20.04.LTS           |
+------------------------------+---------------------+
| Python                       | 3.8                 |
+------------------------------+---------------------+
| OpenCV                       | 4.1.0               |
+------------------------------+---------------------+
| GStreamer                    | 1.16.3              |
+------------------------------+---------------------+
| Cmake                        | 3.16.5              |
+------------------------------+---------------------+
| Ninja                        | 1.10.0              |
+------------------------------+---------------------+
| Meson                        | 0.53.2              |
+------------------------------+---------------------+
| Jupyterlab                   | 3.0.14              |
+------------------------------+---------------------+
| NeoAI - DLR                  | 1.10.0              |
+------------------------------+---------------------+
| Tensorflow                   | TIDL_PSDK_8.5       |
+------------------------------+---------------------+
| TFLite-runtime               | TIDL_PSDK_8.5       |
+------------------------------+---------------------+
| ONNX-runtime                 | TIDL_PSDK_8.5       |
+------------------------------+---------------------+
| PyYAML                       | 5.3.1               |
+------------------------------+---------------------+
| TI Model Zoo                 | 8.5.0               |
+------------------------------+---------------------+
| edgeai-gst-plugins           | 0.8.0               |
+------------------------------+---------------------+
| edgeai-tiovx-modules         | 8.5                 |
+------------------------------+---------------------+
| edgeai-dl-inferer            | 8.5                 |
+------------------------------+---------------------+
| edgeai-tidl-tools            | 08_05_00_11         |
+------------------------------+---------------------+
