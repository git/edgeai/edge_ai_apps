.. _pub_edgeai_running_simple_demos:

====================
Running Simple demos
====================

This chapter describes how to run Python and C++ demo applications in
`edge_ai_apps <https://git.ti.com/cgit/edgeai/edge_ai_apps>`_ with live
camera and display.

.. note::

    Please note that the Python demos are useful for quick prototyping
    while C++ demos are similar by design but tuned for performance.

.. _pub_edgeai_python_demos:

Running Python based demo applications
======================================

Python based demos are simple executable scripts written for image
classification, object detection and semantic segmentation. Demos are
configured using a YAML file. Details on configuration file parameters can
be found in :ref:`pub_edgeai_configuration`

Sample configuration files for out of the box demos can be found in
``edge_ai_apps/configs`` this folder also contains a template config file
which has brief info on each configurable parameter ``edge_ai_apps/configs/app_config_template.yaml``

Here is how a Python based image classification demo can be run,

.. code-block:: bash

    root@tda4vm-sk:/opt/edge_ai_apps/apps_python# ./app_edgeai.py ../configs/image_classification.yaml

The demo captures the input frames from connected USB camera and passes
through pre-processing, inference and post-processing before sent to display.
Sample output for image classification and object detection demos are as below,

.. |logo1| image:: ./images/edgeai-image-classify.jpg
   :align: middle
.. |logo2| image:: ./images/edgeai-object-detect.jpg
   :align: middle

+---------+---------+
| |logo1| | |logo2| |
+---------+---------+

To exit the demo press Ctrl+C.

.. _pub_edgeai_cpp_demos:

Running C++ based demo applications
===================================

C++ apps are cross compiled while packaging, they can be directly tested as
given below

.. code-block:: bash

    root@tda4vm-sk:/opt/edge_ai_apps/apps_cpp# ./bin/Release/app_edgeai ../configs/image_classification.yaml

To exit the demo press Ctrl+C.

C++ apps can be modified and built on the target as well using below steps

.. code-block:: bash

    root@tda4vm-sk:/opt/edge_ai_apps/apps_cpp# rm -rf build bin lib
    root@tda4vm-sk:/opt/edge_ai_apps/apps_cpp# mkdir build
    root@tda4vm-sk:/opt/edge_ai_apps/apps_cpp# cd build
    root@tda4vm-sk:/opt/edge_ai_apps/apps_cpp/build# cmake ..
    root@tda4vm-sk:/opt/edge_ai_apps/apps_cpp/build# make -j2

.. note::

   Both Python and C++ applications are similar by construction and can accept
   the same config file and command line arguments

.. note::
   The C++ apps built on Yocto Linux may not run in Docker as there could be
   a mismatch in Glib and other related tools. So its **highly recommended** to
   rebuild the C++ apps within the Docker environment. **setup_script.sh** can
   be run to rebuild and install all components when switching from docker to
   yocto and vice versa

List of Deep Learning Based Demos supported OOB
===============================================

- **Image Classification**: Detects top 5 most approximate classes in the
  Imagenet dataset for the given input frame
- **Object Detection**: Detects and draws bounding boxes around the objects,
  Also classifies the objects to one of the classes in coco dataset
- **Semantic Segmentation**: Classifies each pixel into calss in ade20k dataset
- **Human Pose Estimation**: Detects the posture of all the persons in the
  frame and draws a skeleton approximating the pose
- **Face Detection**: Detects the face and draws bounding box around the face
